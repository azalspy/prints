import scrapy

class HealthDrug(scrapy.Item):
    link = scrapy.Field()
    name = scrapy.Field()

    info = scrapy.Field()
    code = scrapy.Field()
    mole = scrapy.Field()

    brand = scrapy.Field()
    famil = scrapy.Field()
    maker = scrapy.Field()

    type = scrapy.Field()
    pack = scrapy.Field()
    cons = scrapy.Field()
    ages = scrapy.Field()

    etat = scrapy.Field()
    cnss = scrapy.Field()
    sale = scrapy.Field()
    pcep = scrapy.Field()
    tier = scrapy.Field()
    gros = scrapy.Field()

    prix_c = scrapy.Field()
    prix_v = scrapy.Field()
    prix_h = scrapy.Field()
    prix_r = scrapy.Field()

    attr = scrapy.Field()

class SacredText(scrapy.Item):
    orig = scrapy.Field()
    path = scrapy.Field()
    type = scrapy.Field()

    when = scrapy.Field()
    name = scrapy.Field()
    link = scrapy.Field()

###################################################################################################

class SggItem(scrapy.Item):
    title = scrapy.Field()
    data = scrapy.Field()

class SgggovmaLaw(scrapy.Item):
    orig = scrapy.Field()
    path = scrapy.Field()
    type = scrapy.Field()

    when = scrapy.Field()
    name = scrapy.Field()
    link = scrapy.Field()

class AdalaItem(scrapy.Item):
    fqdn = scrapy.Field()
    link = scrapy.Field()
    prnt = scrapy.Field()

    name = scrapy.Field()
    unit = scrapy.Field()

    docx = scrapy.Field()
    p_ar = scrapy.Field()
    p_fr = scrapy.Field()

###################################################################################################

class MantoojRecord(scrapy.Item):
    orig = scrapy.Field()
    path = scrapy.Field()
    type = scrapy.Field()

    when = scrapy.Field()
    name = scrapy.Field()
    link = scrapy.Field()

###################################################################################################

class Offensive_Server(scrapy.Item):
    orig = scrapy.Field()
    path = scrapy.Field()
    type = scrapy.Field()

    when = scrapy.Field()
    name = scrapy.Field()
    link = scrapy.Field()

class Offensive_Mobile(scrapy.Item):
    orig = scrapy.Field()
    path = scrapy.Field()
    type = scrapy.Field()

    when = scrapy.Field()
    name = scrapy.Field()
    link = scrapy.Field()

class Offensive_Device(scrapy.Item):
    orig = scrapy.Field()
    path = scrapy.Field()
    type = scrapy.Field()

    when = scrapy.Field()
    name = scrapy.Field()
    link = scrapy.Field()

